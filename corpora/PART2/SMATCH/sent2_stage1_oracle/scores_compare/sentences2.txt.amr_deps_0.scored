# ::snt A self-contained hardware or software component that interacts with a larger system
# ::term module
# ::sntId 152
# ::treeId 0
# ::src jamr
# ::F1 0.625
(h / hardware 
      :mod (s / self-contained) 
      :op1-of (o / or 
            :ARG1-of (i / interact-01 
                  :ARG0 (s3 / system 
                        :mod (l / large))) 
            :op2 (c / component 
                  :mod (s2 / software))))

# ::snt A facility that holds servers and related network equipment
# ::term datacenter
# ::sntId 170
# ::treeId 0
# ::src jamr
# ::F1 0.643
(a / and 
      :ARG0-of (h / hold-01 
            :ARG1 (s / server) 
            :ARG2 (f / facility)) 
      :op1 (e / equipment 
            :ARG1-of (r / relate-01) 
            :mod (n / network)))

# ::snt To remove a file from the disk
# ::term delete
# ::sntId 190
# ::treeId 0
# ::src jamr
# ::F1 0.833
(r / remove-01 
      :ARG0 (d / disk) 
      :ARG1 (f / file))

# ::snt An organization that sets international standards , founded in 1946
# ::term iso
# ::sntId 200
# ::treeId 0
# ::src jamr
# ::F1 0.769
(f / found-01 
      :ARG0 (o / organization 
            :ARG0-of (s / set-02 
                  :ARG1 (s2 / standard 
                        :mod (i / international)))) 
      :ARG1 (d / date-entity 
            :year 1946))

# ::snt An HTTP command used to retrieve data from a Web server
# ::term get
# ::sntId 158
# ::treeId 0
# ::src jamr
# ::F1 0.714
(u / use-01 
      :ARG0 (c / command) 
      :ARG1 (h / http) 
      :ARG2 (r / retrieve-01 
            :ARG0 (s / server 
                  :mod (w / web)) 
            :ARG1 (d / data)))

# ::snt A device that converts sound waves into analogous electrical waves
# ::term microphone
# ::sntId 207
# ::treeId 0
# ::src jamr
# ::F1 0.857
(d / device 
      :ARG0-of (c / convert-01 
            :ARG1 (w2 / wave 
                  :mod (s / sound)) 
            :ARG2 (a / analogous 
                  :cause (w / wave 
                        :mod (e / electricity)))))

# ::snt Data that are turned into unreadable text in order to prevent unauthorized viewing
# ::term encrypted
# ::sntId 186
# ::treeId 0
# ::src jamr
# ::F1 0.800
(d / data 
      :ARG0-of (p / prevent-01 
            :ARG1 (v / view-01 
                  :ARG1 (u / unreadable) 
                  :ARG1-of (a / authorize-01 
                        :polarity -)) 
            :purpose-of (t / turn-02 
                  :ARG1 (t2 / text))))

# ::snt An asynchronous communications protocol used to quickly transmit files over high-quality lines
# ::term fast
# ::sntId 145
# ::treeId 0
# ::src jamr
# ::F1 0.700
(u / use-01 
      :ARG0 (p / protocol) 
      :ARG1 (c / communicate-01 
            :ARG1 (a / asynchronous)) 
      :ARG2 (t / transmit-01 
            :ARG0 (l / line 
                  :mod (q2 / quality 
                        :mod (h / high))) 
            :ARG1 (f / file) 
            :manner (q / quick)))

# ::snt The communications protocol used to connect to Web servers on the Internet or on a local network
# ::term http
# ::sntId 150
# ::treeId 0
# ::src jamr
# ::F1 0.700
(u / use-01 
      :ARG0 (p / protocol) 
      :ARG1 (c / communicate-01 
            :ARG0 (n / network 
                  :mod (l / local))) 
      :ARG2 (t / connect-01 
            :ARG1 (s / server 
                  :location (o / or 
                        :op1 (i / internet)) 
                  :poss (w / web))))

# ::snt The rules governing the structure of a programming language
# ::term syntax
# ::sntId 195
# ::treeId 0
# ::src jamr
# ::F1 0.900
(s / structure 
      :ARG1-of (g / govern-01 
            :ARG0 (r / rule)) 
      :poss (l / language 
            :mod (p / program)))

# ::snt A symbol used to point to some element on screen
# ::term pointer
# ::sntId 198
# ::treeId 0
# ::src jamr
# ::F1 0.600
(p / point-01 
      :ARG1 (e / element) 
      :ARG2 (s2 / screen) 
      :ARG2-of (u / use-01 
            :ARG0 (s / symbol)))

# ::snt An FTP command used to download a file from a server or to display the contents of a text file
# ::term get
# ::sntId 157
# ::treeId 0
# ::src jamr
# ::F1 0.773
(u / use-01 
      :ARG0 (c / command 
            :mod (f / ftp)) 
      :ARG1 (o / or 
            :op1 (d / download-01 
                  :ARG1 (f3 / file 
                        :poss (s / server))) 
            :op2 (d2 / display-01 
                  :ARG0 (f2 / file 
                        :mod (t / text)) 
                  :ARG1 (c2 / content))))

# ::snt A device that adapts one type of signal to another
# ::term modem
# ::sntId 127
# ::treeId 0
# ::src jamr
# ::F1 0.545
(a / adapt-01 
      :ARG0 (d / device) 
      :ARG1 (t / type 
            :location (s / signal)) 
      :ARG2 (a2 / another))

# ::snt The symbol used to point to some element on screen
# ::term cursor
# ::sntId 176
# ::treeId 0
# ::src jamr
# ::F1 0.583
(p / point-01 
      :ARG0 (s2 / some) 
      :ARG1 (e / element) 
      :ARG2 (s3 / screen) 
      :ARG2-of (u / use-01 
            :ARG0 (s / symbol)))

# ::snt A software control panel that enables the user to configure the appearance or actions in an application , operating system or the hardware
# ::term settings
# ::sntId 160
# ::treeId 0
# ::src jamr
# ::F1 0.667
(a / appear-01 
      :ARG0 (o / or) 
      :ARG1 (o2 / operate 
            :op1-of (a2 / application) 
            :op1-of (h / hardware) 
            :op1-of (s / system)) 
      :ARG1-of (c2 / configure-01 
            :ARG0 (p / panel 
                  :ARG0-of (c / control-01) 
                  :ARG0-of (e / enable-01 
                        :ARG1 (p2 / person 
                              :ARG0-of (u / use-01))))))

# ::snt A program that helps the user analyze data
# ::term tool
# ::sntId 177
# ::treeId 0
# ::src jamr
# ::F1 0.750
(h / help-01 
      :ARG0 (p / program) 
      :ARG2 (p2 / person 
            :ARG0-of (u / use-01) 
            :ARG0-of (a / analyze-01 
                  :ARG1 (d / data))))

# ::snt Software that is installed into an existing application in order to enhance its capability
# ::term plug-in
# ::sntId 172
# ::treeId 0
# ::src jamr
# ::F1 0.560
(c / capable-41 
      :ARG1-of (e2 / enhance-01 
            :time-of (i / install-01 
                  :ARG0 (s / software) 
                  :ARG1 (a / application 
                        :ARG1-of (e / exist-01)))))

# ::snt A list of parameters read by a program in order to modify its behavior
# ::term profile
# ::sntId 204
# ::treeId 0
# ::src jamr
# ::F1 0.560
(b / behave-01 
      :ARG1 (p / parameter) 
      :ARG1-of (m / modify-01 
            :ARG0 (l / list 
                  :ARG0-of (r / read-01 
                        :ARG1 (p2 / program)))))

# ::snt A discipline for developing software that emphasizes customer involvement and teamwork
# ::term xp
# ::sntId 154
# ::treeId 0
# ::src jamr
# ::F1 0.625
(e / emphasize-01 
      :ARG0 (s / software 
            :ARG1-of (d2 / develop-02 
                  :ARG0 (d / discipline))) 
      :ARG1 (i / involve-01 
            :ARG1 (c / customer)) 
      :ARG2 (a / and 
            :op1 (t / teamwork)))

# ::snt A statement that requests services from another subroutine or program
# ::term call
# ::sntId 122
# ::treeId 0
# ::src jamr
# ::F1 0.688
(s / state-01 
      :ARG0 (s2 / service 
            :cause (r / request-01 
                  :ARG0 (p / program 
                        :op2-of (o / or 
                              :op1 (s3 / subroutine) 
                              :quant (a / another))))) 
      :ARG1 (t / thing))

# ::snt It is used to separate parts of an application from each other and to separate one application from another
# ::term window
# ::sntId 118
# ::treeId 0
# ::src jamr
# ::F1 0.765
(u / use-01 
      :ARG0 (a / and 
            :op1 (s2 / separate-01 
                  :ARG0 (a4 / application) 
                  :ARG1 (p / part)) 
            :op2 (s / separate-01 
                  :ARG1 (a3 / application))) 
      :ARG1 (i / it))

# ::snt An address that points to a Web page or other file on a Web server
# ::term link
# ::sntId 120
# ::treeId 0
# ::src jamr
# ::F1 0.778
(p / point-01 
      :ARG0 (s / server 
            :mod (w / web)) 
      :ARG1 (a / address) 
      :ARG2 (p2 / page 
            :mod (w2 / web) 
            :op1-of (o / or 
                  :op2 (f / file 
                        :mod (o2 / other)))))

# ::snt An optical device that reads a printed page and that converts it into a graphics image for the computer
# ::term scanner
# ::sntId 193
# ::treeId 0
# ::src jamr
# ::F1 0.811
(c / convert-01 
      :ARG0 (d / device 
            :ARG0-of (r / read-01 
                  :ARG1 (p / page 
                        :ARG1-of (p2 / print-01))) 
            :mod (o / optics)) 
      :ARG1 (i / image 
            :mod (c2 / computer) 
            :mod (g / graphic)))

# ::snt An independent block of data , text or graphics that was created by a separate application
# ::term object
# ::sntId 116
# ::treeId 0
# ::src jamr
# ::F1 0.895
(b / block 
      :ARG0-of (d / depend-01 
            :ARG1 (c / create-01 
                  :ARG0 (a / application 
                        :mod (s / separate)) 
                  :ARG1 (o / or 
                        :op1 (d2 / data) 
                        :op2 (t / text) 
                        :op3 (g / graphic))) 
            :polarity -))

# ::snt An on-screen list of available actions that a user can perform in a software program
# ::term menu
# ::sntId 125
# ::treeId 0
# ::src jamr
# ::F1 0.750
(p3 / possible 
      :domain (p / perform-02 
            :ARG0 (p2 / person 
                  :ARG0-of (u / use-01 
                        :ARG1 (a / action 
                              :consist (l / list 
                                    :mod (s / screen)) 
                              :mod (a2 / available)))) 
            :ARG1 (p4 / program 
                  :mod (s2 / software))))

# ::snt The metal interconnecting pathways on a chip that tie the transistors , resistors and capacitors together
# ::term track
# ::sntId 141
# ::treeId 0
# ::src jamr
# ::F1 0.600
(a / and 
      :ARG1-of (t / tie-01 
            :ARG0 (c / chip) 
            :ARG2 (t2 / transistor) 
            :manner (t3 / together)) 
      :op1 (r / resistor) 
      :op2 (c2 / capacitor 
            :ARG0-of (i / interconnect-01 
                  :ARG1 (m / metal) 
                  :cause-of (p / pathway))))

# ::snt A very popular message broadcasting system that lets anyone send alphanumeric text messages to a list of followers
# ::term twitter
# ::sntId 196
# ::treeId 0
# ::src jamr
# ::F1 0.808
(s2 / send-01 
      :ARG0 (s / system 
            :ARG0-of (b / broadcast-01 
                  :ARG1 (m2 / message)) 
            :ARG0-of (l / let-01 
                  :ARG1 (a / anyone)) 
            :mod (p / popular 
                  :degree (v / very))) 
      :ARG1 (m / message) 
      :ARG2 (l2 / list 
            :ARG1-of (f / follow-02 
                  :ARG0 (p2 / person))) 
      :manner (a2 / alphanumeric))

# ::snt To input into the computer from a peripheral device or the network
# ::term read
# ::sntId 115
# ::treeId 0
# ::src jamr
# ::F1 0.833
(i / input-01 
      :ARG1 (c / computer) 
      :concession (o / or 
            :op1 (d / device 
                  :mod (p / peripheral)) 
            :op2 (n / network)))

# ::snt A pointer embedded within a record that refers to data or the location of data in another record
# ::term link
# ::sntId 121
# ::treeId 0
# ::src jamr
# ::F1 0.773
(p / point-01 
      :ARG0 (t / thing 
            :ARG1-of (e / embed-01 
                  :ARG0 (r3 / record 
                        :mod (a / another)) 
                  :ARG2 (r / record 
                        :ARG0-of (r2 / refer-01 
                              :ARG1 (d2 / data 
                                    :op1-of (o / or 
                                          :op2 (l / location 
                                                :mod (d / data)))))))))

# ::snt Software that performs a very specific and repetitive function
# ::term engine
# ::sntId 129
# ::treeId 0
# ::src jamr
# ::F1 0.786
(a / and 
      :op1 (s2 / specific 
            :degree (v / very)) 
      :op2 (f / function-01 
            :ARG1 (r / repetitive) 
            :ARG1-of (p / perform-02 
                  :ARG0 (s / software))))

# ::snt An organization that issues digital certificates and makes its public key widely available to its intended audience
# ::term ca
# ::sntId 130
# ::treeId 0
# ::src jamr
# ::F1 0.739
(i2 / intend-01 
      :ARG1 (a2 / audience 
            :mod-of (a / available 
                  :ARG2-of (i / issue-01 
                        :ARG0 (o / organization 
                              :ARG0-of (m / make-02 
                                    :ARG1 (k / key 
                                          :mod (p / public)))) 
                        :ARG1 (c / certificate 
                              :mod (d / digital))) 
                  :mod (w / wide))))

# ::snt A network device that forwards data packets from one network to another
# ::term router
# ::sntId 133
# ::treeId 0
# ::src jamr
# ::F1 0.733
(d / device 
      :ARG0-of (f / forward-01 
            :ARG1 (p / packet 
                  :quant (d2 / data)) 
            :ARG2 (a / another) 
            :time (n2 / network)) 
      :mod (n3 / network))

# ::snt Laser light can be focused down to a tiny spot as small as a single wavelength
# ::term laser
# ::sntId 137
# ::treeId 0
# ::src jamr
# ::F1 0.650
(f / focus-01 
      :ARG0 (s3 / single 
            :mod-of (w / wavelength 
                  :location-of (s2 / small))) 
      :ARG1 (l / light 
            :mod (l2 / laser)) 
      :ARG2 (s / spot 
            :mod (t / tiny)) 
      :domain-of (p / possible))

# ::snt A small , marked area on an electronic device that is physically pressed down to activate a function
# ::term button
# ::sntId 139
# ::treeId 0
# ::src jamr
# ::F1 0.667
(a2 / activate-01 
      :ARG1 (f / function 
            :ARG0-of (m / mark-02 
                  :ARG1 (a / area 
                        :mod (s / small)))) 
      :ARG1-of (p / press-01 
            :ARG0 (d / device 
                  :mod (e / electronic)) 
            :time (p2 / physically)))

# ::snt The title of an on-screen button that is clicked in order to start some action such as a search
# ::term go
# ::sntId 148
# ::treeId 0
# ::src jamr
# ::F1 0.688
(t / title 
      :ARG0-of (s4 / search-01 
            :ARG2-of (s2 / start-01 
                  :ARG1 (a / action 
                        :mod (s3 / some)) 
                  :time-of (c / click-01 
                        :ARG1 (b / button 
                              :mod (s / screen))))))

# ::snt A screen display technology developed in 1963 at the David Sarnoff Research Center in Princeton , NJ
# ::term lcd
# ::sntId 151
# ::treeId 0
# ::src jamr
# ::F1 0.667
(s / screen 
      :mod-of (d / display 
            :mod-of (t / technology 
                  :op1 (d2 / develop-02 
                        :ARG0 (s3 / state 
                              :location-of (c / city 
                                    :name (n2 / name 
                                          :op1 "Princeton")) 
                              :name (n3 / name 
                                    :op1 "NJ")) 
                        :ARG1 (o / organization 
                              :name (n / name 
                                    :op1 "Center" 
                                    :op2 "Research" 
                                    :op3 "Sarnoff" 
                                    :op4 "David")) 
                        :time (d3 / date-entity 
                              :year 1963)))))

# ::snt A group of disk or tape records that is stored as a single unit
# ::term block
# ::sntId 155
# ::treeId 0
# ::src jamr
# ::F1 0.562
(s / store-01 
      :ARG0 (r / record 
            :op4-of (o / or 
                  :op1 (g / group) 
                  :op2 (d / disk) 
                  :op3 (t / tape))) 
      :ARG1 (u / unit 
            :mod (s2 / single)))

# ::snt A group of bits or bytes that is transmitted as a single unit
# ::term block
# ::sntId 156
# ::treeId 0
# ::src jamr
# ::F1 0.786
(t / transmit-01 
      :ARG0 (u / unit 
            :mod (s2 / single)) 
      :ARG1 (g / group 
            :location (b / bit 
                  :op1-of (o / or 
                        :op2 (b2 / byte)))))

# ::snt To store data locally in order to speed up subsequent retrievals
# ::term cache
# ::sntId 161
# ::treeId 0
# ::src jamr
# ::F1 0.833
(s / store-01 
      :ARG0 (l / local) 
      :ARG1 (d / data) 
      :purpose (s2 / speed-02 
            :ARG1 (r / retrieve-01 
                  :ARG1 (s3 / subsequent))))

# ::snt Reserved areas of memory in every computer that are used to speed up instruction execution , data retrieval and data updating
# ::term cache
# ::sntId 162
# ::treeId 0
# ::src jamr
# ::F1 0.679
(r / reserve-01 
      :ARG0 (m / memory) 
      :ARG1 (a / area) 
      :ARG2 (c / computer 
            :ARG1-of (u / use-01 
                  :ARG2 (s / speed-02 
                        :ARG0 (e / every) 
                        :ARG1 (e2 / execute-02 
                              :ARG1 (i / instruction)) 
                        :ARG2 (a2 / and 
                              :op1 (r2 / retrieve-01 
                                    :ARG1 (d2 / data)) 
                              :purpose (u2 / update-01 
                                    :ARG1 (d / data)))))))

# ::snt Verifying the identity of a user logging into a network
# ::term authentication
# ::sntId 163
# ::treeId 0
# ::src jamr
# ::F1 0.800
(v / verify-01 
      :ARG1 (i / identity 
            :poss (u / user 
                  :ARG1-of (l / log-05 
                        :ARG2 (n / network)))))

# ::snt A storage device that converts chemical energy into electrical energy
# ::term battery
# ::sntId 164
# ::treeId 0
# ::src jamr
# ::F1 0.857
(d / device 
      :ARG0-of (s / store-01 
            :ARG1 (e3 / electricity 
                  :mod-of (e / energy))) 
      :ARG0-of (c / convert-01 
            :ARG1 (e2 / energy 
                  :mod (c2 / chemical))))

# ::snt The audio compression technology that revolutionized digital music
# ::term mp3
# ::sntId 165
# ::treeId 0
# ::src jamr
# ::F1 0.917
(r / revolutionize-05 
      :ARG0 (t / technology 
            :mod (c / compression) 
            :mod (a / audio)) 
      :ARG1 (m / music 
            :mod (d / digital)))

# ::snt A common method for keeping track of data so that it can be accessed quickly
# ::term index
# ::sntId 171
# ::treeId 0
# ::src jamr
# ::F1 0.727
(p / possible 
      :domain (a / access-01 
            :manner (q / quick)) 
      :purpose-of (k / keep-03 
            :ARG0 (m / method 
                  :mod (c / common)) 
            :ARG1 (t / track-01 
                  :ARG1 (d / data))))

# ::snt The world's largest database and enterprise software vendor founded in 1977 by Larry Ellison
# ::term oracle
# ::sntId 174
# ::treeId 0
# ::src jamr
# ::F1 0.607
(a / and 
      :op1 (d / database) 
      :op2 (v / vend-01 
            :ARG0 (l / large 
                  :degree (m / most)) 
            :ARG2 (f / found-01 
                  :ARG0 (w / world) 
                  :ARG2 (p / person 
                        :name (n / name 
                              :op1 "Ellison" 
                              :op2 "Larry")) 
                  :time (d2 / date-entity 
                        :year 1977)) 
            :mod (s / software 
                  :mod (e / enterprise))))

# ::snt A group of fields added to the beginning of the message
# ::term header
# ::sntId 180
# ::treeId 0
# ::src jamr
# ::F1 0.600
(b / begin-01 
      :ARG0 (g / group 
            :ARG0-of (a / add-02 
                  :ARG1 (f / field))) 
      :ARG1 (m / message))

# ::snt To replace existing software or hardware with a newer version
# ::term upgrade
# ::sntId 183
# ::treeId 0
# ::src jamr
# ::F1 0.667
(r / replace-01 
      :ARG0 (s / software) 
      :ARG1 (o / or 
            :op1 (h / hardware 
                  :ARG1-of (e / exist-01 
                        :ARG0 (n / new 
                              :mod-of (v / version))))))

# ::snt The glass or plastic elements that focus light onto sensor in a video camera
# ::term lens
# ::sntId 184
# ::treeId 0
# ::src jamr
# ::F1 0.778
(f / focus-01 
      :ARG0 (e / element 
            :mod (p / plastic) 
            :op2-of (o / or 
                  :op1 (g / glass))) 
      :ARG1 (l / light) 
      :ARG2 (s / sensor 
            :poss (c / camera 
                  :mod (v / video))))

# ::snt A person that designs software
# ::term developer
# ::sntId 189
# ::treeId 0
# ::src jamr
# ::F1 0.833
(d / design-01 
      :ARG0 (p / person) 
      :ARG1 (s / software))

# ::snt To identify a block of text in order to perform some task on it such as deletion , copying and moving
# ::term mark
# ::sntId 197
# ::treeId 0
# ::src jamr
# ::F1 0.833
(m / move-01 
      :op3-of (a / and 
            :op1 (d / delete-01 
                  :ARG2-of (p / perform-02 
                        :ARG0 (t / text) 
                        :ARG1 (t2 / task) 
                        :purpose-of (i / identify-01 
                              :ARG1 (b / block)))) 
            :op2 (c / copy-01)))

# ::snt The part of a network that handles the major traffic
# ::term backbone
# ::sntId 199
# ::treeId 0
# ::src jamr
# ::F1 1.000
(p / thing 
      :ARG0-of (h / handle-01 
            :ARG1 (t / traffic 
                  :mod (m / major))) 
      :part-of (n / network))

# ::snt To convert any coded content to the required format for display or printing
# ::term render
# ::sntId 201
# ::treeId 0
# ::src jamr
# ::F1 0.833
(c3 / convert-01 
      :ARG1 (c / content 
            :ARG1-of (c2 / code-01) 
            :mod (a / any)) 
      :ARG2 (f / format 
            :ARG1-of (d / display-01 
                  :op1-of (o / or 
                        :op2 (p / print-01))) 
            :ARG1-of (r / require-01)))

# ::snt An arithmetic expression that solves a problem
# ::term formula
# ::sntId 211
# ::treeId 0
# ::src jamr
# ::F1 0.875
(p / problem 
      :ARG1-of (s / solve-01 
            :ARG0 (e / expression 
                  :mod (a / arithmetic))))

# ::snt A programming language written by Larry Wall that combines syntax from several Unix utilities and languages
# ::term perl
# ::sntId 217
# ::treeId 0
# ::src jamr
# ::F1 0.692
(a / and 
      :ARG0-of (c / combine-01 
            :ARG1 (s / syntax) 
            :purpose (w / write-01 
                  :ARG0 (p2 / person 
                        :name (n / name 
                              :op1 "Wall" 
                              :op2 "Larry")) 
                  :ARG1 (l / language 
                        :mod (p / program)))) 
      :op1 (u / utility 
            :mod (u2 / unix) 
            :quant (s2 / several 
                  :quant-of (l2 / language))))

# ::snt An HTML layout feature that renders multiple documents on a Web page at the same time
# ::term frames
# ::sntId 143
# ::treeId 0
# ::src jamr
# ::F1 0.750
(f / feature 
      :mod (l / layout 
            :ARG0-of (r / render-03 
                  :ARG1 (d / document) 
                  :ARG2 (p / page 
                        :mod (w / web)) 
                  :ARG3 (t / time 
                        :ARG2-of (s / same-41 
                              :ARG0 (m / multiple)))) 
            :mod (h / html)))

# ::snt Software that converts a set of high-level language statements into a lower-level representation
# ::term compiler
# ::sntId 181
# ::treeId 0
# ::src jamr
# ::F1 0.950
(s / software 
      :ARG0-of (c / convert-01 
            :ARG1 (s2 / set 
                  :consist-of (s3 / statement 
                        :mod (l2 / language 
                              :mod (l3 / level 
                                    :mod (h / high))))) 
            :ARG2 (r / representation 
                  :mod (l / level 
                        :mod (l4 / low)))))

# ::snt Ahe action taken by the transmitting station to establish a connection with the receiving station in a dial-up network
# ::term call
# ::sntId 123
# ::treeId 0
# ::src jamr
# ::F1 0.629
(d / dial-up 
      :ARG0-of (c / connect-01 
            :ARG1 (s2 / station 
                  :ARG0-of (r / receive-01)) 
            :ARG1-of (e / establish-01 
                  :ARG0 (s / station 
                        :ARG1-of (t / transmit-01)))) 
      :mod-of (n / network))

# ::snt Replacing manual operations with electronics and computer-controlled devices
# ::term automation
# ::sntId 194
# ::treeId 0
# ::src jamr
# ::F1 0.875
(a / and 
      :op1 (e / electronics) 
      :op2 (d / device 
            :ARG1-of (c / control-01 
                  :ARG0 (c2 / computer)) 
            :ARG2-of (r / replace-01 
                  :ARG1 (o / operation 
                        :mod (m / manual)))))

# ::snt Software from Apple that is widely used to organize media on the computer as well as iPods , iPhones and iPads
# ::term itunes
# ::sntId 182
# ::treeId 0
# ::src jamr
# ::F1 0.760
(a / and 
      :op1 (o / organization 
            :ARG1-of (u / use-01 
                  :ARG0 (c / computer) 
                  :ARG2 (o2 / organize-01 
                        :ARG0 (w / wide) 
                        :ARG1 (m / media))) 
            :location-of (s / software) 
            :name (n / name 
                  :op1 "Apple")) 
      :op2 (i1 / ipod) 
      :op3 (i2 / iphone) 
      :op4 (i3 / ipad))

# ::snt The Apple device that revolutionized mobile computing and caused an explosion of smartphones worldwide
# ::term iphone
# ::sntId 144
# ::treeId 0
# ::src jamr
# ::F1 0.952
(c2 / cause-01 
      :ARG0 (d / device 
            :ARG0-of (r / revolutionize-05 
                  :ARG1 (c / compute-01 
                        :mod (m / mobile))) 
            :poss (o / organization 
                  :name (n / name 
                        :op1 "Apple"))) 
      :ARG1 (e / explode-02 
            :ARG1 (s / smartphone) 
            :location (w / worldwide)))

# ::snt A monetary amount that is added to an account balance
# ::term credit
# ::sntId 159
# ::treeId 0
# ::src jamr
# ::F1 0.900
(a2 / add-02 
      :ARG1 (a / amount 
            :mod (m / monetary)) 
      :ARG2 (b / balance 
            :mod (a3 / account)))

# ::snt A machine that provides arithmetic capabilities
# ::term calculator
# ::sntId 213
# ::treeId 0
# ::src jamr
# ::F1 0.875
(c / capable-41 
      :ARG1-of (p / provide-01 
            :ARG0 (m / machine)) 
      :ARG2 (a / arithmetic))

# ::snt A computer company that became a software company and then merged into Apple
# ::term next
# ::sntId 119
# ::treeId 0
# ::src jamr
# ::F1 0.947
(b / become-01 
      :ARG1 (c / company 
            :ARG1-of (m / merge-01 
                  :ARG2 (o / organization 
                        :name (n / name 
                              :op1 "Apple")) 
                  :time (t / then)) 
            :mod (c2 / computer)) 
      :ARG2 (c3 / company 
            :mod (s / software)))

# ::snt The library routines are linked into the program when it is compiled
# ::term library
# ::sntId 167
# ::treeId 0
# ::src jamr
# ::F1 0.762
(c / compile-01 
      :ARG1 (p / program 
            :ARG3-of (l / link-01 
                  :ARG1 (r / routine 
                        :mod (l2 / library)))))

