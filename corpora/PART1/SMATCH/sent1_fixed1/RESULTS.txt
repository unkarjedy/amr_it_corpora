Smatch evaluations
Gold AMRs file: sentences1_fixed1.txt.amr_gold

amr_deps_gold VS amr_gold
Precision: 0.576
Recall: 0.541
Document F-score: 0.558

amr_deps_0 VS amr_gold
Precision: 0.581
Recall: 0.545
Document F-score: 0.563

amr_deps_1 VS amr_gold
Precision: 0.569
Recall: 0.533
Document F-score: 0.551

amr_deps_2 VS amr_gold
Precision: 0.563
Recall: 0.528
Document F-score: 0.545

amr_deps_3 VS amr_gold
Precision: 0.567
Recall: 0.531
Document F-score: 0.548

amr_deps_4 VS amr_gold
Precision: 0.567
Recall: 0.532
Document F-score: 0.549
